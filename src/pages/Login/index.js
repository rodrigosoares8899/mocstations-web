import React from 'react';
import firebase from '../../firebase';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import {makeStyles, ThemeProvider} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
//import Link from '@material-ui/core/Link';
import {Link} from 'react-router-dom';
import myTheme from '../../Theme';
import StyledGradientButton from '../../components/StyledGradientButton';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: myTheme.palette.primary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const classes = useStyles();


  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}> 
        <Avatar className={classes.avatar}>
          <Icon>account_box</Icon>
        </Avatar>
        <Typography variant="h5">Login</Typography>
        <form className={classes.form}>
          <ThemeProvider theme={myTheme}>
            <TextField
              variant="outlined"
              fullWidth={true}
              id="email"
              name="email"
              autoComplete="email"
              required
              margin="normal"
              label="Email Address"
            />
            <TextField
              variant="outlined"
              fullWidth={true}
              id="password"
              name="password"
              autoComplete="current-password"
              required
              margin="normal"
              label="Password"
              type="password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Manter-me conectado"
            />
          </ThemeProvider>
         
          <StyledGradientButton
            className={classes.submit}
            type="submit"
            fullWidth={true}
            variant="contained"
            children="Fazer Login"
          />
          <Grid container>
            <Grid item xs>
              <Typography variant="body2">
                <Link to="/register">Ainda não é usuário ? Cadastre-se</Link>
              </Typography>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}