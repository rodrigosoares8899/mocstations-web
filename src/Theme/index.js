import React from 'react';
import orange from '@material-ui/core/colors/orange';
import lightGreen from '@material-ui/core/colors/lightGreen';

import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
      primary: {
          main: orange[700]
      },
      secondary: {
        main: lightGreen[500],
      },
    },
  });


export default theme