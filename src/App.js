import React, {Component} from 'react';
import firebase from './firebase';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Login from './pages/Login';
import Register from './pages/Register';
import MenuBar from './components/MenuBar';
import Home from './pages/Home';
import CircularProgress from '@material-ui/core/CircularProgress';

class App extends Component {

    state = {
        firebaseInitialized: false
    }

    componentDidMount() {
        firebase.isInitialized().then(result => {
            this.setState({
                firebaseInitialized: result
            })
        })
    }

    render(){
        return  <MenuBar/> && this.state.firebaseInitialized !== false ? (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route exact path="/register" component={Register} />
                    <Route exact path='/home' component={Home} />
                </Switch>
            </BrowserRouter>
        )
        :
        (
            <div style={{ 
                display: "flex", 
                flexDirection: "column",
                alignItems: "center",
                marginTop: "206px"
            }}>
                <CircularProgress />
            </div>
        )
    }

}

export default App;
